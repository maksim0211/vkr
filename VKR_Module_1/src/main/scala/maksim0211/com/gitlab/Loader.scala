package maksim0211.com.gitlab

import org.joda.time.DateTime
import org.jsoup.Jsoup

import java.io.{FileInputStream, FileOutputStream}
import java.net.{HttpURLConnection, URL}
import java.util.zip.ZipInputStream
import scala.language.postfixOps
import scala.sys.process._

object Loader {

  //Скачивание файлов по ссылке
  private def loadFilesFromUrl(url: String, filename: String) = {
    val req = List("curl", "-L",
      url,
      "--speed-limit", "100000",
      "-H", "\"Content-type: application/zip\"",
      "-o", filename)
    println(s"Start download new zip in ${DateTime.now()} to path: ${filename}")
    req.!!
    println(s"Finish download new zip in ${DateTime.now()} to path: ${filename}")

    //Перестало работать в связи с изменением логики сервера у USPTO
    //new URL(url) #> new File(filename) !!
  }

  //Получить имена скачиваемых архивов
  private def loadFileNames(url: String): List[String] = {
    println(s"Downloading list of zip-archive names...")
    val doc = Jsoup.parse(s"curl --speed-limit 100000 $url" !!)
    doc.select("tr").select("td").select("a").text().split(" ").map(_.trim).toList
  }

  //Метод распаковки архива
  private def unzipFile(zipName: String, resourceDir: String): Unit = {
    val fis = new FileInputStream(s"$resourceDir$zipName")
    val zis = new ZipInputStream(fis)
    println(s"Start unzip in ${DateTime.now()}")
    Stream.continually(zis.getNextEntry).takeWhile(_ != null).foreach { file =>
      val fout = new FileOutputStream(s"$resourceDir${file.getName}")
      val buffer = new Array[Byte](1024)
      Stream.continually(zis.read(buffer)).takeWhile(_ != -1).foreach(fout.write(buffer, 0, _))
    }
    println(s"Finish unzip in ${DateTime.now()}")
  }

  /*
    Главный метод класса
    Принимается ссылка на страницу с архивами, и путь к директории для сохранения распакованных файлов
    Принимаются имена архивов с сайта
    Для каждого файла происходит скачивание в директорию
    Распаковка в ту же директорию (архив не удаляется программой)
  */
  def loadFilesToResourcesDir(parameters: Parameters) {
    val ref = parameters.getReference()
    val resourceDir = parameters.getDirOfResources()
    val listFileNames = Loader.loadFileNames(ref)
    listFileNames.foreach({
      name =>
        val start = System.currentTimeMillis()
        loadFilesFromUrl(s"$ref$name", s"$resourceDir$name")
        unzipFile(s"$name", s"$resourceDir")
        println((System.currentTimeMillis() - start) / 1000 + s" seconds for file $name")
    })
    println("Success!")
  }

  private def loadFilesFromUrl2(urln: String, filename: String) = {
    val url = new URL(urln);
    val connection = url.openConnection().asInstanceOf[HttpURLConnection];
    connection.setRequestMethod("GET");
    val in = connection.getInputStream();
    val zipIn = new ZipInputStream(in);
    var entry = zipIn.getNextEntry();

    while (entry != null) {
      System.out.println(entry.getName());
      if (!entry.isDirectory()) {
        // if the entry is a file, extracts it
        System.out.println("===File===");
        zipIn.closeEntry();
        entry = zipIn.getNextEntry();
      }
    }
  }
}
