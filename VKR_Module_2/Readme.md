# VKR

### VKR_Module_2

### Репозиторий дипломного проекта по разработке программного продукта по генерации патентного ландшафта. Модуль 2.

### Создатель : Савельев Максим

### Используется Python 3.8.3 и классические библиотеки: os/csv/datetime, также библиотека BeautifulSoap4

### Функционал

Данный модуль последовательно выполняет следующие задачи:
- Первая подзадача (функция split):
  - Функция принимает путь к файлу, в которой лежат патенты за неделю (конечный файл, например, называется "example_name_dir/ipg200618.xml")
  - Открывает этот файл, и делит его на файлы меньшего размера (один файл - один патент)
  - Каждый файл сохраняет в созданную папку example_name_dir/ipg200618 (зависит от названия большого файла)
  - Возвращает строковое значение - путь к созданной папке
- Вторая подзадача (функция highlight_necessary_info)
  - Функция принимает путь к папке, созданной функцией split
  - Для каждого файла в этой папке ищутся значения:
    - тега `us-bibliographic-data-grant.classifications-ipcr.classification-ipcr.section`
    - тега `us-bibliographic-data-grant.classifications-ipcr.classification-ipcr.class`
    - тега `us-bibliographic-data-grant.classifications-ipcr.classification-ipcr.subclass`
    - тега `us-bibliographic-data-grant.classifications-ipcr.classification-ipcr.main-group`
  - Если условие `section == "G" and classname == "06" and subclass == "F"` выполняется, то далее ищутся все значения тега `decription.p`
  - Данные инкрементально записываются в csv файл с 5 колонками, section,class,subclass,main-group,text
  - Результатом работы 2 подзадачи является наполненный csv файл