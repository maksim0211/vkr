import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.col

object Merger {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local")
      .appName("MergerDataset")
      .getOrCreate()

    val df_1 = spark.read.parquet("C:\\Users\\maksi\\Documents\\VKR_FOLDER\\vkr\\VKR_Module_3\\dataset_a61full")
    val df_2 = spark.read.parquet("C:\\Users\\maksi\\Documents\\VKR_FOLDER\\files\\parqetdataset")

    df_1.show(3)

    println(df_1.count())
    println(df_2.count())

    df_1
      .select(col("main-group") as "label", col("text"))
      .limit(100000)
      .union(df_2
        .select(col("main-group") as "label", col("text"))
        .limit(100000))
      .repartition(1)
      .write
      .parquet("C:\\Users\\maksi\\Documents\\VKR_FOLDER\\vkr\\VKR_Module_3\\parquet_dataset_GA_200k_v2")

    spark.close()

  }

}
