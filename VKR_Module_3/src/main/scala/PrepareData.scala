import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, count, desc, row_number, udf}
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.joda.time.LocalDateTime

object PrepareData {
  def csvToParquet(args : Array[String]) : Unit = {

    //Определяем две udf
    val udfReplaceBrackets = udf((str : String) => str.replace("("," ").replace(")", " "))
    val udfCountWordInText = udf((str : String) => str.split(" ").length)

    val sc = SparkSession.builder()
      .appName("csvtoparq")
      .master("local")
      .config("spark.driver.memory", "8g")
      .config("spark.driver.cores", "4")
      .config("spark.memory.offHeap.enabled", "true")
      .config("spark.memory.offHeap.size", "6g")
      .config("spark.driver.maxResultSize", "10g")
      .getOrCreate()

    sc.sparkContext.setLogLevel("WARN")

    import sc.implicits._

    //Читаем csv
    val data = sc.sqlContext.read
      .format("csv")
      .option("charset","cp1251")
      .option("header", "false")
      .option("mode", "DROPMALFORMED") // Drop empty/malformed rows
      .load(args(0))

    println("СSV Загружен." + LocalDateTime.now())

    //Получение датафрейма
    val res = data
      .distinct()
      .filter($"_c3".cast("int").isNotNull)
      .filter(col("_c4").isNotNull)
      .select(col("_c3").as("main-group"),
        col("_c2").as("subclass"),
        udfReplaceBrackets(col("_c4")).as("text"))
      //колонка с числом слов в тексте
      .withColumn("count_word_in_text", udfCountWordInText(col("text")))
      //колонка с числом уникальных main-group
      .withColumn("cntr",count("main-group").over(Window.partitionBy("main-group")))
      //патенты с самым "богатым" описанием будут вверху датафрейма
      .orderBy(desc("count_word_in_text"))

    // Убрать колонку
    val res2 = res
      .drop($"cntr")
/*
    //Вывести информацию о полученном датасете
    val res_analytic = res2.select("main-group","subclass")
      .withColumn("count", count("main-group").over(Window.partitionBy("subclass")))
      .select("subclass","count")
      .distinct()

    val res_analytic_2 = res2.select("main-group","subclass")
      .withColumn("count", count("main-group").over(Window.partitionBy("main-group","subclass")))
      .select("subclass", "main-group", "count")
      .distinct()

    res_analytic
      .repartition(1)
      .write
      .format("csv")
      .mode("overwrite")
      .save("C:\\Users\\maksi\\Documents\\VKR_FOLDER\\vkr\\VKR_Module_3\\src\\main\\csv_analytic_1_g06")

    res_analytic_2
      .repartition(1)
      .write
      .format("csv")
      .mode("overwrite")
      .save("C:\\Users\\maksi\\Documents\\VKR_FOLDER\\vkr\\VKR_Module_3\\src\\main\\csv_analytic_2_g06")
*/
    println(s"СSV Отфильтрован. Размер датафрейма = ${res2.count()}. ${LocalDateTime.now()}")

    //Сформировать его
    res2
      .repartition(1)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(args(2))

    println(s"Датафрейм записан. ${LocalDateTime.now()}")
  }

  def getNecessaryDataset(args : Array[String], spark : SparkSession, maingroupArray : Array[Int], subclass : String, limit : Long) : DataFrame ={
    //Читаем датасет, фильтруем по main-group, формируем равное число патентов из разных категорий
    import spark.implicits._
    spark.read.format("parquet").load(args(2))
      .filter($"subclass" === subclass)
      .filter($"main-group".cast(IntegerType).isin(maingroupArray:_*))
      .withColumn("cnt", row_number().over(Window.partitionBy("main-group").orderBy(col("main-group"), desc("count_word_in_text"))))
      //.withColumn("cnt", row_number().over(Window.partitionBy("main-group").orderBy(desc("count_word_in_text"))))
      .withColumnRenamed("main-group","label")
      //.orderBy(desc("count_word_in_text"))
      //.limit(5000)
      .filter($"cnt" < limit)
      //.drop($"cnt")
  }
}
