import PrepareData.{csvToParquet, getNecessaryDataset}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.row_number
import org.apache.spark.sql.{SaveMode, SparkSession}

object Main {
  def main(args: Array[String]): Unit = {

    //1 аргумент - откуда брать csv
    //2 аргумент - минимальное количество патентов для категории
    //3 аргумент - куда класть parquet
    //4 аргумент - куда сохранять датасет с леммами
    //5 аргумент - необходимо ли преобразовывать csvToParquet => 1 - да/0 - нет
/*
    Пример app_args:
    "C:\Users\maksi\Documents\VKR_FOLDER\files\restest2.csv"
    5000
    "D:\\IJ Projects\\VKR\\VKR_MODULE_3\\parquet_dataset"
    "D:\\IJ Projects\\VKR\\VKR_MODULE_3\\parquet_normalize_text"
    0

 */
    //"C:\Users\maksi\Documents\VKR_FOLDER\vkr\VKR_Module_2\restesta61.csv"  5000  "C:\Users\maksi\Documents\VKR_FOLDER\vkr\VKR_Module_3\dataset_A61"     "C:\Users\maksi\Documents\VKR_FOLDER\vkr\VKR_Module_3\dataset_A61_5k" 0

    //"C:\Users\maksi\Documents\VKR_FOLDER\vkr\VKR_Module_2\restesta61.csv"  100000  "C:\Users\maksi\Documents\VKR_FOLDER\files"     "C:\Users\maksi\Documents\VKR_FOLDER\vkr\VKR_Module_3\dataset_G06F" 0

    val spark = SparkSession
      .builder()
      .master("local")
      .appName("VKR_MODULE_3_1")
      .getOrCreate()

    // Конвертируем csv в parquet
    if (args(4).equals("1")) csvToParquet(args)
    // Создание датасета для работы 4-го модуля

    val dataset = getNecessaryDataset(args, spark, Array(8,9), "K", 30)
      .withColumn("id", row_number().over(Window.orderBy("label", "text")))

    println(s"~~~~>${dataset.count()}")
    // Запишем получаемый dataset
    dataset
        .repartition(1)
        .write
        .mode(SaveMode.Overwrite)
        .parquet(args(3))

  }
}