# VKR

### VKR_Module_1/2/3/4

### Репозиторий дипломного проекта по разработке программного продукта по генерации патентного ландшафта.

### Создатель : Савельев Максим

#### VKR_MODULE_1:

- Scala 2.11;
- Модуль скачивает архивы по url и распаковывает в необходимые директории.

#### VKR_MODULE_2:

- Python 3.8.0;
- Модуль сплитит каждый файл массива патентов за неделю на единичные, парсит, записывает в сводный csv.

#### VKR_MODULE_3:

- Scala 2.11;
- Модуль формирует из csv файла необходимый dataset формата parquet, конфигурируемый пользователем.

#### VKR_MODULE_4:

- Python 3.8.0;
- Модуль трансформирует данные для обучения LDA модели и визуализирует результаты (в jupyter-notebbok).